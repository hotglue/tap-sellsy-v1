"""SellsyV1 tap class."""

from typing import List

from singer_sdk import Stream, Tap
from singer_sdk import typing as th

from tap_sellsy_v1.streams import (
    InvoiceItemssStream,
    InvoicesStream,
    ItemStream,
    PurchaseOrderItemsStream,
    PurchaseOrdersStream,
    SellsyV1Stream,
)

STREAM_TYPES = [
    ItemStream,
    InvoicesStream,
    InvoiceItemssStream,
    PurchaseOrdersStream,
    PurchaseOrderItemsStream,
]


class TapSellsyV1(Tap):
    """SellsyV1 tap class."""

    name = "tap-sellsy-v1"

    config_jsonschema = th.PropertiesList(
        th.Property(
            "consumer_key",
            th.StringType,
            required=True,
        ),
        th.Property(
            "consumer_secret",
            th.StringType,
            required=True,
        ),
        th.Property(
            "access_token",
            th.StringType,
            required=True,
        ),
        th.Property(
            "access_token_secret",
            th.StringType,
            required=True,
        ),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    TapSellsyV1.cli()
