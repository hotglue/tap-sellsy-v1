import json
from typing import Any, Dict, Iterable, Optional, cast , Callable
import requests
import logging
from oauthlib.oauth1 import SIGNATURE_PLAINTEXT
from requests_oauthlib import OAuth1
from singer_sdk.helpers.jsonpath import extract_jsonpath
from singer_sdk.streams import RESTStream
from singer_sdk.authenticators import APIAuthenticatorBase
from singer_sdk.exceptions import RetriableAPIError
import backoff

class SellsyV1Stream(RESTStream, APIAuthenticatorBase):

    url_base = "https://apifeed.sellsy.com/0"

    records_jsonpath = "$.response[*]"
    next_page_token_jsonpath = "$.response.infos[*]"
    extra_retry_statuses = [429, 104]

    @property
    def http_headers(self) -> dict:
        headers = {}
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")

        return headers

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:

        if self.next_page_token_jsonpath:
            all_matches = extract_jsonpath(
                self.next_page_token_jsonpath, response.json()
            )
            first_match = next(iter(all_matches), None)
            next_page_token = first_match
        else:
            next_page_token = response.headers.get("X-Next-Page", None)
        if next_page_token is None:
            return None

        if "pagenum" in next_page_token:
            next_page_token.update({"pagenum": next_page_token["pagenum"] + 1})
            if next_page_token["pagenum"] > next_page_token["nbpages"]:
                return None

        return next_page_token

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        params: dict = {}
        if next_page_token:
            params["pagination"] = next_page_token

        return params

    def prepare_request_payload(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Optional[dict]:

        if self.rest_method is "GET":
            return None

        payload = {}
        payload["request"] = 1
        payload["io_mode"] = "json"
        payload["do_in"] = {"method": self.method_name, "params": self.stream_params}
        if self.tap_stream_id == "invoice_items":
            payload["do_in"]["params"].update(
                self.get_child_params(context, "docid", "invoice_id")
            )

        if self.tap_stream_id == "purchase_order_items":
            payload["do_in"]["params"].update(
                self.get_child_params(context, "id", "order_id")
            )

        payload["do_in"]["params"].update(self.get_url_params(context, next_page_token))
        payload["do_in"] = json.dumps(payload["do_in"])

        return payload

    def parse_response(self, response: requests.Response) -> Iterable[dict]:

        for record in extract_jsonpath(self.records_jsonpath, input=response.json()):
            for row in record.values():
                yield row

    @property
    def generate_oauth(self):
        oauth = OAuth1(
            self.config.get("consumer_key"),
            client_secret=self.config.get("consumer_secret"),
            resource_owner_key=self.config.get("access_token"),
            resource_owner_secret=self.config.get("access_token_secret"),
            signature_type="auth_header",
            signature_method=SIGNATURE_PLAINTEXT,
        )
        return oauth
    
    def prepare_request(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> requests.PreparedRequest:
    
        http_method = self.rest_method
        url: str = self.get_url(context)
        params: dict = self.get_url_params(context, next_page_token)
        request_data = self.prepare_request_payload(context, next_page_token)
        headers = self.http_headers

        authenticator = self.authenticator
        if authenticator:
            headers.update(authenticator.auth_headers or {})
            params.update(authenticator.auth_params or {})

        request = cast(
            requests.PreparedRequest,
            self.requests_session.prepare_request(
                requests.Request(
                    method=http_method,
                    url=url,
                    params=params,
                    headers=headers,
                    data=request_data,
                    auth=self.generate_oauth
                ),
            ),
        )
        return request

    def get_child_params(self, context, param_id, param_val):
        params = {}
        paritions_len = len(context)
        if paritions_len > 0:
            return {param_id: context.get(param_val)}
        return {}

    def request_decorator(self, func: Callable) -> Callable:
        """Instantiate a decorator for handling request failures."""
        decorator: Callable = backoff.on_exception(
            backoff.expo,
            (
                RetriableAPIError,
                requests.exceptions.ReadTimeout,
                requests.exceptions.ConnectionError,
                requests.exceptions.ChunkedEncodingError,
            ),
            max_tries=8,
            factor=2,
        )(func)
        return decorator
    
    def _request(
        self, prepared_request: requests.PreparedRequest, context: dict 
    ) -> requests.Response:
       
        response = self.requests_session.send(prepared_request, timeout=self.timeout)
        if response.status_code == 401:
            self.logger.info("OAuth authorization attempt was unsuccessful.")
            prepared_request = self.prepare_request(context, None)
            response = self.requests_session.send(prepared_request, timeout=self.timeout)

        self._write_request_duration_log(
            endpoint=self.path,
            response=response,
            context=context,
            extra_tags={"url": prepared_request.path_url}
            if self._LOG_REQUEST_METRIC_URLS
            else None,
        )
        self.validate_response(response)
        logging.debug("Response received successfully.")
        return response
    
     